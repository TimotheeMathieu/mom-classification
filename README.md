# MOM Classification 
MOM Classification is a Python module developed using algorithms presented in the article [arxiv:1808.03106][MOMRM_arxiv]. 

This code is made of several algorithms to do robust classification using Median of Means risk minimization. The different codes are modeled from the Logistic Regression, the Perceptron and the Kernel Logistic Regression.

###  Dependencies
1. NUMPY
2. Scikit-Learn

For running the examples Matplotlib is required.

### Basic example
Using for example the logistic regression

    
    from linear_model import logregMOM
    # we use a logistic regression MOM, with 7 blocks, a step-size parameterized with eta0=0.1, 
    # a L^2 regularization of beta=0.01 and a multi-class classification scheme one vs one.
    lr=logregMOM(K=7,eta0=0.1,beta=0.01,multi='ovo')
    from sklearn import datasets
    from sklearn.model_selection import cross_val_score
    import numpy as np
    df=datasets.load_iris()
    print(np.mean(cross_val_score(lr,df.data,df.target,cv=10)))

    
### Example
We provide a notebook (Illustration_notebook.ipynb) with examples from the [article][MOMRM_arxiv]. 

It is viewable directly from the bitbucket source page [here][notebook] or it can be run locally.

Before running the notebook make sure that the dependencies are installed (Matplotlib and scikit-learn are available through pip). The notebook have been tested only with Python3.

[notebook]: https://bitbucket.org/TimotheeMathieu/mom-classification/src/master/Illustration_notebook.ipynb?viewer=nbviewer 
[MOMRM_arxiv]: https://arxiv.org/abs/1808.03106
